from discord.ext import commands
from discord.ext import tasks
from discord import TextChannel, DMChannel

from typing import Union, List


class BaseCog(commands.Cog):

    EMBED_CHAR_LIMIT = 1024

    def __init__(self, bot: commands.Bot) -> None:
        self.bot = bot

        async def send_text_message(self,
                                ctx: Union[commands.Context, TextChannel, DMChannel, int],
                                text: str
                                ) -> None:
        """
        Sends an arbitrarily long string as one or more messages to a channel.
        String is split into multiple messages if length of string exceeds 
        Discord text message character limit.
        
        Parameters
        ----------
        ctx : `commands.Context`
            Discord Context
        text : `str`
            String to post to channel
        channel_id : `Optional[int]`, optional
            Optional channel ID if target channel is not part of the context.
        """
        channel: Union[commands.Context, TextChannel, DMChannel] = None

        # Obtain channel
        if isinstance(ctx, int):
            channel = self.bot.get_channel(channel_id)
        elif isinstance(ctx, (commands.Context, TextChannel, DMChannel)):
            channel = ctx
        else:
            raise discord.DiscordException("Argument 'ctx' must be a Discord Context, text channel or a channel ID.")

        # Split string into chunks
        chunks = await self._split_string_to_chunks(text)

        # Send chunked messages
        for chunk in chunks:
            await channel.send(chunk)

    async def _split_string_to_chunks(self, text: str, limit: int=None) -> List[str]:
        """Splits a string into (default: 1024) char long chunks."""
        if not limit or limit>self.CHAR_LIMIT:
            limit = self.CHAR_LIMIT
        return [text[i:i+limit] for i in range(0, len(text), limit)]

    async def _split_string_by_lines(self, text: str, limit: int=None, strict: bool=False) -> List[str]:
        """Splits a string into `limit`-sized chunks. DEFAULT: 1024
        
        The string is split into n<=limit sized chunks based on 
        occurences of newline chars, whereas `BaseCog._split_string_to_chunks()` 
        splits string into n<=limit chunks, with no regard for splitting
        words or sentences based on newline chars.
        """
        if not limit or limit > self.EMBED_CHAR_LIMIT:
            limit = self.EMBED_CHAR_LIMIT
        
        if len(text) < limit: # no need to split
            return [text]
        
        chunk = "" # Lines in a chunk
        chunk_len = 0 # Size of current chunk
        chunks = []
        
        for line in text.splitlines(keepends=True):
            line_len = len(line)
            
            # Handle lines whose length exceeds limit
            if line_len > limit:
                if strict:
                    raise discord.DiscordException(
                        "Unable to split string. Line length exceeds limit!"
                    )
                else: # Fall back on _split_string_to_chunks()
                    return await self._split_string_to_chunks(text, limit)
            
            if chunk_len + line_len > limit:
                chunks.append(chunk)
                chunk = ""
                chunk_len = 0
            
            chunk += line
            chunk_len += line_len
        
        else:
            if chunk:
                chunks.append(chunk)
        
        return chunks