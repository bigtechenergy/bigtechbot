"""
BUNCHA PSEUDO CODE HERE NOW
"""

from discord.ext import commands
from discord.ext import tasks
from osmon import OSMon

from .base_cog import BaseCog

class MonitorCog(BaseCog):
    def __init__(self, bot: commands.Bot) -> None:
        self.bot = bot
        self.monitor = OSMon(some_config_file_path_here)

    @tasks.loop(minutes=5)
    async def ping_servers(self) -> None:
        await self.check_servers()
    
    async def check_servers(self) -> None:
        failed = await self.monitor.check()
        if not failed:
            return
        await self._report_failed(failed)
    
    async def _report_failed(self, failed: List[OpenStackServers]) -> None:
        await self.send_text_message(
            self.bot.config.report_channel, 
            "The following servers failed: xxxxxxxxxxx"
            "These measures were taken: yyyyyyyyyyyyyyy"
        )