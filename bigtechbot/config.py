from __future__ import annotations

import yaml
from pathlib import Path
from dataclasses import dataclass

# String concatenation
def join(loader, node) -> str:
    seq = loader.construct_sequence(node)
    return ''.join(str(i) for i in seq)

# Parse value as Path
def path(loader, node) -> Path:
    seq = loader.construct_sequence(node)
    return Path('/'.join(str(i) for i in seq))


## register the tag handler
yaml.add_constructor('!join', join)
yaml.add_constructor('!path', path)



@dataclass
class Config:
    bot_id: int
    bot_token: str
    bot_secret: str
    openstack_credentials: OpenStackCredentials


    @classmethod
    def from_dict(cls, d: dict) -> Config:




def load_config(path: str="config.yml") -> dict:
    with open(path, "r") as f:
        config = yaml.load(f)
    return config
